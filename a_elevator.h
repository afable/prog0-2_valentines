//
//  a_elevator.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-04.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_ELEVATOR_H_
#define A_ELEVATOR_H_

#include "a_block.h"


// init values for elevators
const float ELEVATOR_SIDE_LENGTH = 1.0f;
const float ELEVATOR_RADIUS = ELEVATOR_SIDE_LENGTH * SIN45 + ERROR_RANGE_SMALL;
const float ELEVATOR_HEIGHT = 1.0f;
const float ELEVATOR_TRANS_SPEED = 0.02f;
const float ELEVATOR_ROT_SPEED = 1.0f;



/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AElevator: moving blocks that make up moving platforms within game. these elevators have a
 *|		constant speed and are always moving. directions are reversed on collision with a block.
 *|
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- ABlock: a static block that makes up the ground terrain and island terrain.
 *|
 *|				- AElevator: moving blocks that make up moving platforms within game.
 *|
 *|					- ABoat: a special kind of elevator: moving blocks that are player controlled transport vessels.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class AElevator : public ABlock
{
private:
	
public:
	AElevator(void) : ABlock()
	{
		
	}
	
	virtual ~AElevator(void)
	{
		
	}
	
	/* VIRTUAL FUNCTION REDEFINITIONS */
	virtual void Init(int elevatorId, float xloc, float yloc, float zloc, float height, float boxSideLen, float radius, float transSpeed, float rotSpeed, float xrot, float yrot)
	{
		nId = elevatorId;
		
		// elevator position, size, and orientation
		vLocation.Set(xloc, yloc, zloc, 1.0f);
		vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		fHalfHeight = height / 2.0f;
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		fRotX = xrot;
		fRotY = yrot;
		
		// collision data
		boundingBox.Init(vLocation, vForward, vRight, boxSideLen);
		boundingCircle.Init(vLocation, radius);
		
		// velocities and speeds
		fGravity = ::ZERO_GRAVITY;
		fTransSpeed = transSpeed;
		fForwardVelocity = 0.0f;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		//fRightVelocity = 0.0f;
		//fLeftVelocity = 0.0f;
		fRotSpeed = rotSpeed;
		//fRotXVelocity = 0.0f;
		fRotYVelocity = 0.0f;
		
		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		//bEdge = false;
		//bCorner = false;
		nSquished = ::SQUISH_AMOUNT_MAX;
		
		// jump data
		//bCanJump = false;
		//nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		//bHit = false;
		// set reset values
		fXLoc = xloc;
		fYLoc = yloc;
		fZLoc = zloc;
		fXRot = xrot;
		fYRot = yrot;
	}
	
	void Reset(void)
	{
		//nId = eyeId;	// means not initialized
		
		// actor position, size, and orientation
		vLocation.Set(fXLoc, fYLoc, fZLoc, 1.0f);
		//vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		//vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		//CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		//fHalfHeight = height / 2.0f;
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		fRotX = fXRot;
		fRotY = fYRot;
		
		// collision data
		boundingBox.Init(vLocation, vForward, vRight, ::ELEVATOR_SIDE_LENGTH);
		boundingCircle.Init(vLocation, ::ELEVATOR_RADIUS);
		
		// velocities and speeds
		fGravity = ::ZERO_GRAVITY;
		//fTransSpeed = transSpeed;
		fForwardVelocity = 0.0f;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		fRightVelocity = 0.0f;
		fLeftVelocity = 0.0f;
		//fRotSpeed = rotSpeed;
		fRotXVelocity = 0.0f;
		fRotYVelocity = 0.0f;
		
		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		bEdge = false;
		bCorner = false;
		nSquished = ::SQUISH_AMOUNT_MAX;
		
		// jump data
		bCanJump = false;
		nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		bHit = false;
	}
	
	// process incoming events: handle events (key input) and set object velocities
	void ProcessEvents(SDL_Event& event)
	{
		// after grabbing an event from the event queue
		switch (event.type)
		{		
			case SDL_KEYDOWN: switch (event.key.keysym.sym)
			{
				case SDLK_KP1: fUpVelocity = fTransSpeed;
					break;
				case SDLK_KP0:	fDownVelocity = NEGATIVE(fTransSpeed);
					break;
					
				default:
					break;
			}
				break;
				
			case SDL_KEYUP: switch (event.key.keysym.sym)
			{
				case SDLK_KP1: fUpVelocity = 0.0f;
					break;
				case SDLK_KP0: fDownVelocity = 0.0f;
					break;
					
				default:
					break;
			}
				break;
				
			case SDL_MOUSEBUTTONDOWN: // do something if mouse is clicked?
				break;
				
			default:
				break;
		}
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| ELEVATOR 2 BLOCK COLLISIONS IN ROTATION Y,Y,X,Z
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	void ProcessElevator2BlockCollisionRotY(AActor* block)
	{
		/**************************************************
		 * ELEVATOR TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS ROTY: collisions with blocks outter bounding circle
		 **************************************************/
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{			
			/**************************************************
			 * ELEVATOR TO BLOCK CIRCLE-TO-SQUARE COLLISIONS ROTY: collisions with blocks inner bounding box
			 **************************************************/
			while ( Square2SquareCollision(boundingBox, block->boundingBox) )
			{				
				bCollision = true;
				
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// elevator at level that can interact with block
				{
					// get vector from elevator to block
					Subtract(pusher2pushee, vLocation, block->vLocation);
					
					// translate elevator in x, z away from block
					MoveDirectionX(pusher2pushee, ::COLLISION_SPEED);
					MoveDirectionZ(pusher2pushee, ::COLLISION_SPEED);
					boundingBox.Update(vLocation, vForward, vRight);	// update the box location and sidelen
					boundingCircle.Update(vLocation);	// update circle's center
				}
				else	// elevator not at level that can interact with block
				{
					// do nothing and allow the y rotation
					break;	// while ( Square2SquareCollision(boundingBox, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from elevator[" << nId << "] to block[" << block->nId << "] roty\n";
					 )
					block->Reset();
					Reset();
					break;
				}
			}
		}
	}
	
	void ProcessElevator2BlockCollisionY(AActor* block)
	{
		// test if block bounding circles are close
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{		
			/**************************************************
			 * TEST BLOCK CIRCLE-TO-SQUARE COLLISIONS Y: collisions with blocks inner bounding box
			 **************************************************/
			while ( Square2SquareCollision(boundingBox, block->boundingBox) )
			{
				bCollision = true;
				
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// evelator at level that can interact with block
				{
					// translate back in y
					MoveUpY(NEGATIVE(absv(fUpVelocity + fDownVelocity + fGravity)));	
					fTopY = vLocation[1] + fHalfHeight;
					fBotY = vLocation[1] - fHalfHeight;
				}
				else	// elevator not at level that can interact with block
				{
					// do nothing and allow the y translation
					break;	// while ( Square2SquareCollision(boundingBox, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from elevator[" << nId << "] to block[" << block->nId << "] y\n";
					 )
					block->Reset();
					Reset();
					break;
				}
			}
		}
	}
	
	void ProcessElevator2BlockCollisionX(AActor* block)
	{
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{
			/**************************************************
			 * ELEVATOR TO BLOCK CIRCLE-TO-SQUARE COLLISIONS X: collisions with blocks inner bounding box
			 **************************************************/
			while ( Square2SquareCollision(boundingBox, block->boundingBox) )
			{
				bCollision = true;
				
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// evelator at level that can interact with block
				{
					// translate back in x
					MoveForwardX(NEGATIVE(fForwardVelocity + fBackwardVelocity));
					MoveRightX(NEGATIVE(fRightVelocity + fLeftVelocity));
					boundingBox.Update(vLocation, vForward, vRight);	// update the box location and sidelen
					boundingCircle.Update(vLocation);	// update circle's center
				}
				else	// elevator not at level that can interact with block
				{
					// do nothing and allow the x translation
					break;	// while ( Square2SquareCollision(boundingBox, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from elevator[" << nId << "] to block[" << block->nId << "] x\n";
					 )
					block->Reset();
					Reset();
					break;
				}
			}
		}
	}
	
	void ProcessElevator2BlockCollisionZ(AActor* block)
	{
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{
			/**************************************************
			 * ELEVATOR TO BLOCK CIRCLE-TO-SQUARE COLLISIONS Z: collisions with blocks inner bounding box
			 **************************************************/
			while ( Square2SquareCollision(boundingBox, block->boundingBox) )
			{
				bCollision = true;
				
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// evelator at level that can interact with block
				{
					// translate back in z
					MoveForwardZ(NEGATIVE(fForwardVelocity + fBackwardVelocity));
					MoveRightZ(NEGATIVE(fRightVelocity + fLeftVelocity));
					boundingBox.Update(vLocation, vForward, vRight);	// update the box location and sidelen
					boundingCircle.Update(vLocation);	// update circle's center
				}
				else	// elevator not at level that can interact with block
				{
					// do nothing and allow the z translation
					break;	// while ( Square2SquareCollision(boundingBox, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from elevator[" << nId << "] to block[" << block->nId << "] z\n";
					 )
					block->Reset();
					Reset();
					break;
				}
			}
		}
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| ELEVATOR 2 EYE/MONSTERS COLLISIONS IN ROTATION Y,Y,X,Z
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	void ProcessElevator2EyeCollisionRotY(AActor* eye)
	{
		// if there is a collision with a elevator's outter bounding circle, then let's test the inner box
		if ( Circle2CircleCollision(boundingCircle, eye->boundingCircle) )
		{
			while ( Circle2SquareCollision(eye->boundingCircle, boundingBox) )
			{
				bCollision = true;
				
				if ( eye->fTopY >= fBotY && eye->fBotY <= fTopY ) // eye at a level that can interact with elevator
				{
					// get a vector from elevator to eye
					Subtract(pusher2pushee, eye->vLocation, vLocation);
					
					// translate eye in x,z direction away from elevator
					eye->MoveDirectionX(pusher2pushee, COLLISION_SPEED);
					eye->MoveDirectionZ(pusher2pushee, COLLISION_SPEED);
					eye->boundingCircle.Update(eye->vLocation);	// update since elevator moved eye
				}
				else	// eye is not at level that can interact with elevator
				{
					// do nothing and allow x,z movements
					break;	// while ( Circle2SquareCollision(eye->boundingCircle, boundingBox) )
				}
				
				--eye->nSquished;
				if ( eye->nSquished < 0 ) // if eye has been squished between two blocks;
				{
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *eye << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from elevator to eye/monster roty\n";
					 )
					eye->Reset();
					break;
				}
			}
		}
	}
	
	void ProcessElevator2EyeCollisionY(AActor* eye)
	{
		// if there is a collision with a elevator's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, eye->boundingCircle) )
		{			
			/**************************************************
			 * ELEVATOR TO EYE CIRCLE-TO-SQUARE COLLISIONS Y: collisions with elevators inner bounding box
			 **************************************************/
			if ( Circle2SquareCollision(eye->boundingCircle, boundingBox) )	// if not while since y logic only happens once a frame and only if there is a collision
			{
				bCollision = true;
				
				/**************************************************
				 * ELEVATOR TO EYE COLLISION Y: since vUp is always pointing in y axis
				 *	direction, y movements will not have any x or z values. since a collision
				 *	exists, we just need to check if eye is above, ontop, level, or below elevator
				 **************************************************/			
				if ( eye->fBotY > fTopY + absv(eye->fDownVelocity + eye->fGravity) )	// eye high above elevator
				{
					// allow the y movement since eye very high from this elevator (already updated)
				}
				else if ( eye->fBotY >= fTopY - absv(fUpVelocity + fDownVelocity + fGravity) )	// else if eye standing ontop of elevator
				{
					if ( bBotCollision )	// if there was already a collision with elevators (e.g. eye standing between two elevators in same y value) don't translate back since it has already been done
					{
						// do nothing since has been handled already
					}
					else	// if this is the first elevator to collide with eye's bottom, translate eye up only if elevator is also translating up
					{
						if ( fUpVelocity + fDownVelocity + fGravity > 0.0f ) // move eye only if elevator moving up
						{
							eye->MoveUpY(fUpVelocity + fDownVelocity + fGravity);	// translate eye in elevator direction
							// update top and bottom boundaries based on eye new height
							eye->fTopY = eye->vLocation[1] + eye->fHalfHeight;
							eye->fBotY = eye->vLocation[1] - eye->fHalfHeight;
							eye->bBotCollision = true;		// now eye on multiple elevators will not move eye up
						}
					}
				}
				else if ( eye->fTopY < fBotY - absv(eye->fGravity) ) // eye far below elevator (gravity negative)
				{
					// allow the y movement since eye very low from this elevator (already updated)
				}
				else if ( eye->fTopY <= fBotY + JUMP_SPEED )	// eye's head butting elevator, bump eye down only if elevator is also moving down
				{
					if ( bTopCollision )	// handle eye headbutting multiple elevators
					{
						// do nothing since handled already
					}
					else	// first time eye is headbutting a elevator in this frame
					{
						if ( fUpVelocity + fDownVelocity + fGravity < 0.0f )	// only push eye down do if elevator moving down
						{
							while ( eye->fTopY >= fBotY )	// handle being squished up
							{
								eye->fUpVelocity = -eye->fUpVelocity;	// change up velocity direction to down!
								eye->MoveUpY(fUpVelocity + fDownVelocity + fGravity);	// want eye to move down from headbutt
								eye->fTopY = eye->vLocation[1] + eye->fHalfHeight;
								eye->fBotY = eye->vLocation[1] - eye->fHalfHeight;
								eye->bTopCollision = true;
								
								--eye->nSquished;
								if ( eye->nSquished < 0 ) // if eye has been squished between two blocks;
								{
									DEBUGGING 
									(
									 std::cerr << *this << std::endl;
									 std::cerr << *eye << std::endl;
									 for (int i=0;i<999;++i) std::cerr << "resetting from elevator to eye/monster y\n";
									 )
									eye->Reset();
									break;
								}
							}
						}
					}
				}
				else	// eye is on same level as elevator
				{
					// should never get here from a y movement
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *eye << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "eye should never get here from y movement y\n";
					 )
				}
			}
		}
	}
	
	void ProcessElevator2EyeCollisionX(AActor* eye)
	{
		// if there is a collision with a elevator's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, eye->boundingCircle) )
		{
			if ( Circle2SquareCollision(eye->boundingCircle, boundingBox) )
			{
				bCollision = true;
				
				if ( eye->fBotY > fTopY + ERROR_RANGE_NORMAL )	// eye high above elevator
				{
					// do not move eye with elevator
				}
				else if ( eye->fTopY >= fBotY ) // eye at a level that can interact with elevator
				{
					// translate eye in the direction block is moving (block moves the eye)
					eye->MoveForwardX(vForward, fForwardVelocity + fBackwardVelocity);
					eye->MoveRightX(vRight, fRightVelocity + fLeftVelocity);
					eye->boundingCircle.Update(eye->vLocation);
					
					// note: squishes handled in eye->ProcessLogic()
				}
				else	// eye not at level that can interact with elevator
				{
					// don't move eye with elevator
				}
			}
		}
	}
	
	void ProcessElevator2EyeCollisionZ(AActor* eye)
	{
		// if there is a collision with a elevator's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, eye->boundingCircle) )
		{
			if ( Circle2SquareCollision(eye->boundingCircle, boundingBox) )
			{
				bCollision = true;
				
				if ( eye->fBotY > fTopY + ERROR_RANGE_NORMAL )	// eye high above elevator
				{
					// do not move eye with elevator
				}
				else if ( eye->fTopY >= fBotY ) // eye at a level that can interact with elevator
				{
					// translate eye in the direction block is moving (block moves the eye)
					eye->MoveForwardZ(vForward, fForwardVelocity + fBackwardVelocity);
					eye->MoveRightZ(vRight, fRightVelocity + fLeftVelocity);
					eye->boundingCircle.Update(eye->vLocation);
					
					// note: squishes handled in eye->ProcessLogic()
				}
				else	// eye not at level that can interact with elevator
				{
					// don't move eye with elevator
				}
			}
		}
	}
	
	// helper function of AElevator::ProcessLogic() that handles the -y boundary
	virtual void ProcessElevatorLogic(const AActor* eye)
	{
		if ( eye->fDownVelocity < -0.05f )	// if eye is falling from sky valley, move elevator down (we always want elevator to be available if eye falls from sky valley)
			fDownVelocity = -0.1f;
		
		if ( vLocation[1] < fYLoc )	// stop elevator at base
			fDownVelocity = 0.0f;
	}
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| PROCESS LOGIC IN ROTATION Y,Y,X,Z: move objects, check collisions (and move back if necessary)
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	void ProcessLogic(AActor* eye, const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators, std::vector<AActor*>& monsters)
	{	
		// process elevator logic
		ProcessElevatorLogic(eye);
		
		// reset unit vectors (leave location alone) and bound angles for sin/cos lookup tables
		LoadIdentity();	
		
		// reset squish amount to maximum
		nSquished = ::SQUISH_AMOUNT_MAX;
		
		// start all collisions as false
		bCollision = false;
		bInteractCollision = false;	// handles eye's upper body colliding with blocks
		bTopCollision = false;	// handles eye ontop of many blocks
		bBotCollision = false;	// handles eye headbutting into many blocks
		bEdge = false;	// tests whether eye is at a block's edge
		bCorner = false;	// tests if eye is at a block's corner (rare to hit only a corner and no edge but possible)
		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| ROTATE IN Y: rotating bounding boxes creates rotational corner collisions. collisions
		 *|		with blocks makes elevator move backwards. colisions with eye and monsters that are
		 *|		at a level that can affect eye/monster moves the monster in elevator's direction
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// add rotational velocities
		fRotY += fRotYVelocity;
		
		// rotate forward and right vectors
		RotateY(fRotY);
		
		// update bounding circles and bounding boxes before checking for collisions
		boundingBox.Update(vLocation, vForward, vRight);	// update the box location and sidelen
		
		/**************************************************
		 * ELEVATOR TO BLOCK COLLISION ROTATION Y
		 **************************************************/
		for ( unsigned int i = 0; i < blocks.size(); ++i )
			ProcessElevator2BlockCollisionRotY(blocks[i]);
		
		/**************************************************
		 * ELEVATOR TO ELEVATOR COLLISION ROTATION Y
		 **************************************************/
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			if ( nId != elevators[i]->nId )		// elevators shouldn't test themselves
				ProcessElevator2BlockCollisionRotY(elevators[i]);
		
		/**************************************************
		 * ELEVATOR TO EYE COLLISIONS ROTATION Y: collisions with elevator's outter bounding circle to eye's
		 *	bounding circle and if pass, test with elevator's inner box to eye's bounding circle.
		 *	there are 2 cases: eye is at a level that can interact with elevator or not at a level
		 *	that can interact with elevator. move eye if collision and can interact.
		 **************************************************/
		ProcessElevator2EyeCollisionRotY(eye);
		
		/**************************************************
		 * ELEVATOR TO MONSTER COLLISIONS ROTATION Y: monsters treated the same way as eye
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i)
			ProcessElevator2EyeCollisionRotY(monsters[i]);
		
		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN Y: move elevator in y, update top and bot variables and test for collisions with
		 *|		the top and bot variables of blocks, eye, and monsters.
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate eye up and down by up, down velocities
		MoveUpY(fUpVelocity + fDownVelocity + fGravity);
		
		// update top and bot variables
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		
		/**************************************************
		 * ELEVATOR TO BLOCK COLLISIONS Y: collisions with blocks outter bounding circle and inner bounding square
		 **************************************************/
		for ( unsigned int i = 0; i < blocks.size(); ++i ) // do this for every block
			ProcessElevator2BlockCollisionY(blocks[i]);
		
		/**************************************************
		 * ELEVATOR TO ELEVATOR COLLISIONS Y: collisions with elevators outter bounding circle and inner bounding square
		 **************************************************/
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			if ( nId != elevators[i]->nId )		// elevators shouldn't test themselves
				ProcessElevator2BlockCollisionY(elevators[i]);
		
		/**************************************************
		 * ELEVATOR TO EYE COLLISIONS Y: collisions elevator's outter bounding circle to eye's
		 *	bounding circle and if pass, test with elevator's inner box to eye's bounding circle.
		 *	move eye if collision.
		 **************************************************/
		ProcessElevator2EyeCollisionY(eye);
		
		/**************************************************
		 * ELEVATOR TO MONSTER COLLISIONS Y: monsters treated the same way as eye
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i)
			ProcessElevator2EyeCollisionY(monsters[i]);

		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN X: move elevator in x, update bounding circle and square, test x collisions with actors
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate forward by forward velocity amount
		MoveForwardX(fForwardVelocity + fBackwardVelocity);
		
		// translate right by right velocity amount
		MoveRightX(fRightVelocity + fLeftVelocity);
		
		// update bounding circles and bounding boxes before checking for collisions
		boundingBox.Update(vLocation, vForward, vRight);	// update the box location and sidelen
		boundingCircle.Update(vLocation);	// update circle's center
		
		/**************************************************
		 * ELEVATOR TO BLOCK COLLISIONS X: test for collisions between elevator and blocks after
		 *	x movements
		 **************************************************/
		for ( unsigned int i = 0; i < blocks.size(); ++i ) // do this for every block
			ProcessElevator2BlockCollisionX(blocks[i]);
		
		/**************************************************
		 * ELEVATOR TO ELEVATOR COLLISIONS X: test for collisions between elevators after x movements
		 **************************************************/
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			if ( nId != elevators[i]->nId )		// elevators shouldn't test themselves
				ProcessElevator2BlockCollisionX(elevators[i]);
		
		/**************************************************
		 * ELEVATOR TO EYE COLLISIONS X: two cases, either eye can interact with
		 *	elevator or eye cannot interact with elevator
		 **************************************************/
		ProcessElevator2EyeCollisionX(eye);
		
		/**************************************************
		 * ELEVATOR TO MONSTER COLLISIONS X: monsters treated the same way as eye
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i)
			ProcessElevator2EyeCollisionX(monsters[i]);

		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| COLLISION TEST IN Z: test block outter bounding circles first and if true, block inner
		 *|		bounding squares and also test eye and monster bounding circles
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		/**************************************************
		 * MOVE AND TEST Z: move elevator in z based on events and check for collisions (and move back if necessary)
		 **************************************************/
		// translate forward by forward velocity amount
		MoveForwardZ(fForwardVelocity + fBackwardVelocity);
		
		// translate right by right velocity amount
		MoveRightZ(fRightVelocity + fLeftVelocity);
		
		// update bounding circles and bounding boxes before checking for collisions
		boundingBox.Update(vLocation, vForward, vRight);	// update the box location and sidelen
		boundingCircle.Update(vLocation);	// update circle's center
		
		/**************************************************
		 * ELEVATOR TO BLOCK COLLISIONS Z: collisions with blocks after z elevator movement
		 **************************************************/
		for ( unsigned int i = 0; i < blocks.size(); ++i ) // do this for every block
			ProcessElevator2BlockCollisionZ(blocks[i]);
		
		/**************************************************
		 * ELEVATOR TO ELEVATOR COLLISIONS Z: collisions with elevators after z movement
		 **************************************************/
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			if ( nId != elevators[i]->nId )		// elevators shouldn't test themselves
				ProcessElevator2BlockCollisionZ(elevators[i]);
		
		/**************************************************
		 * ELEVATOR TO EYE COLLISIONS Z: two cases: either eye at level that can
		 *	interact with elevator or eye not at level that can interact with elevator
		 **************************************************/
		ProcessElevator2EyeCollisionZ(eye);
		
		/**************************************************
		 * ELEVATOR TO MONSTER COLLISIONS Z: monsters treated the same way as eye
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i)
			ProcessElevator2EyeCollisionZ(monsters[i]);
	}
	
	/* FRIEND FUNCTIONS */
	friend std::ostream& operator << (std::ostream& os, const AElevator& el)
	{
		os << "** elevator **: " << el.nId << std::endl;
		os << "\tvLocation: " << el.vLocation << std::endl;
		os << "\tvForward: " << el.vForward << std::endl;
		os << "\tvUp: " << el.vUp << std::endl;
		os << "\tvRight: " << el.vRight << std::endl;
		os << "Velocities (x, y, z): (" << el.fRightVelocity + el.fLeftVelocity << "," << el.fUpVelocity + el.fDownVelocity << "," << el.fForwardVelocity + el.fBackwardVelocity << "), fGravity = " << el.fGravity << "\n";
		os << "Rot (x, y): (" << el.fRotX << ", " << el.fRotY << ")\n";
		os << "boundingBox:\n";
		os << el.boundingBox << std::endl;
		os << "boundingCircle:\n";
		os << el.boundingCircle << std::endl;
		os << "bCollision: " << el.bCollision << std::endl;
		os << "bTopCollision: " << el.bTopCollision << std::endl;
		os << "bBotCollision: " << el.bBotCollision << std::endl;
		os << "bInteractCollision: " << el.bInteractCollision << std::endl;		
		os << "nSquished: " << el.nSquished << std::endl;		
		os << "fTopY: " << el.fTopY << ", fBotY: " << el.fBotY << std::endl;
		
		return os;
	}
	
	
};	// end of class AElevator


#endif	// A_ELEVATOR_H_







