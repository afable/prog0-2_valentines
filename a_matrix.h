//
//  a_matrix.h
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_MATRIX_H_
#define A_MATRIX_H_

#include <iostream>
#include "a_vector.h"

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AMatrix: 4d matrix class for column ordered matrix transformations
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class AMatrix
{
private:
	
	float f[16];  // opengl style matrix, remember: column ordered! (keep that in mind when inputting data and printing out to screen)
	
	/* STATICS */
	static AMatrix temp;	// static matrix used for fast matrix transformations
	static float			// static floats used for fast matrix arithmetic (when temp currently in use and need another temporary matrix)
		a0, a4, a8, a12,
		a1, a5, a9, a13,
		a2, a6, a10, a14,
		a3, a7, a11, a15;
	
	// hardly ever used but called 6 times for matrix transpose (hardly used too)
	inline void Swap(float& f1, float& f2)
	{
		float temp;
		temp = f1;
		f1 = f2;
		f2 = temp;
	}
	
public:
	/* CONSTRUCTOR & DESTRUCTORS */
	AMatrix(void);	// overwrite default constructor
	AMatrix(float m0, float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9, float m10, float m11, float m12, float m13, float m14, float m15);
	AMatrix(const float m[16]);
	AMatrix(const AMatrix& copy);	// overwrite copy constructor
	AMatrix(const AVector& x, const AVector& y, const AVector& z, const AVector& translation=AVector(0.0f, 0.0f, 0.0f, 1.0f));
	~AMatrix(void);		// overwrite default destructor
	
	/* SETTERS */
	void Identity(void);
	void Reset(void);
	void Transpose(void);
	void Zero(void);
	
	/* GETTERS */
	AVector GetTranslation(void);
	AMatrix GetInverse(void);
	float GetDeterminant(const float m[16]);
	bool GenerateInverseMatrix(float i[16], const float m[16]);
	float* GetMatrix(void);
	
	/* ROTATE MATRIX */
	AMatrix& Rotatef(int angle, float x, float y, float z); // simulate glRotatef(angle, x, y, z)
	AMatrix& RotateX(int angle); // right hand rule
	AMatrix& RotateY(int angle);
	AMatrix& RotateZ(int angle);
		
	/* SCALE MATRIX */
	AMatrix& Scalef(float x, float y, float z);
	
	/* TRANSLATE MATRIX */
	AMatrix& Translatef(float x, float y,float z);	// simulate glTranslatef(x, y, z)
	AMatrix& Translate(const float x, const float y, const float z);
	AMatrix& Translate(const AVector& v);
	
	/* OPERATOR OVERLOADS */
	AMatrix& operator = (const AMatrix& other);	// overload default assignment operator
	bool operator == (const AMatrix& other) const;
	bool operator != (const AMatrix& other) const;
	AMatrix& operator *= (const float k);
	AMatrix& operator *= (const AMatrix& m);
	AVector operator * (const AVector& v) const;
	
	float& operator [] (const int x);		// this makes more sense to me (use like an array of floats)
	const float& operator [] (const int x) const;		// this makes more sense to me (use like an array of floats)
	
	/* FRIEND OPERATOR OVERLOADS */
	friend AVector operator * (const AVector& v, const AMatrix& m);
	friend AVector& operator *= (AVector& v, const AMatrix& m);
	friend AMatrix operator * (const AMatrix& m, const float k);
	friend AMatrix operator * (const float k, const AMatrix& m);
	friend AMatrix operator * (const AMatrix& lhm, const AMatrix& rhm);
	friend std::ostream& operator << (std::ostream& os, const AMatrix& m);
	
}; // end of class AMatrix


/* PUBLIC FUNCTIONS */
AMatrix MakePlanarShadowMatrix(const AVector& vPlane, const AVector& vLightPos);


/* EFFICIENT FUNCTIONS */
void BuildSinCosLut(void);
void Multiply(AVector& dst, const AMatrix& m, const AVector& v);



#endif // A_MATRIX_H_







