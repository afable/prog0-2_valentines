/*    
 a_gooch.h
 Dr. Amy Gooch, 2011
 http://webhome.csc.uvic.ca/~agooch/
 */



GLubyte* sea;	// pointer to image for sea texture
GLuint seaTex;	// place to store OpenGL-created names of texture objects
GLubyte* sky;
GLuint skyTex;
GLubyte* platformTop;
GLuint platformTopTex;
GLubyte* platformRest;
GLuint platformRestTex;


GLubyte* ReadPpmFile(char* filename)
{
	FILE* input;
	GLubyte* vol;
	int w, h, max, i, j, k;
	char rgb[3];
	char buffer[200]; 
	
	if ( !(input = fopen(filename,"r")) )
	{
		std::cerr << "Cannot open file: " << filename << ". ReadPpmFile() failed." << std::endl;
		exit(EXIT_FAILURE);
	}
	
	// read a line of input
	fgets(buffer, 200, input);
	if ( strncmp(buffer, "P6", 2) != 0 )
	{
		std::cerr << filename << " is not a binary PPM file! ReadPpmFile() failed." << std::endl;
		exit(EXIT_FAILURE);
	}
	
	// get second line, ignoring comments
	do
	{
		fgets(buffer, 200, input);
	}
	while ( strncmp(buffer, "#", 1) == 0 );
	
	if ( sscanf(buffer, "%d %d", &w, &h) != 2 )
	{
		std::cerr << "Can't read sizes! ReadPpmFile() failed." << std::endl;
		exit(EXIT_FAILURE);
	}
	
	// third line, ignoring comments
	do
	{
		fgets(buffer, 200, input);
	}
	while ( strncmp(buffer, "#", 1) == 0 );
	
	if ( sscanf(buffer, "%d", &max) != 1 )
	{
		std::cerr << "Max size required. ReadPpmFile() failed." << std::endl;
		exit(EXIT_FAILURE);
	}
	std::cerr << filename << " successfully read." << std::endl;
	
	vol = (GLubyte*)malloc( w*h*3*sizeof(GLubyte) );
	
	for ( i=h-1; i>=0; --i )
	{
		for ( j=0; j<w; ++j )
		{
			fread(rgb, sizeof(char), 3, input);
			for ( k=0; k<3; k++ )
				*(vol+i*w*3+j*3+k) = (GLubyte)rgb[k];
		}
	}
	return vol;
}

void InitTextures(GLubyte* pTex, GLuint& texture, char* file)
{
	// read in image for texture
	pTex = ReadPpmFile(file);
	
	// create a texture object
	glGenTextures(1, &texture);
	
	// make it the current texture
	glBindTexture(GL_TEXTURE_2D, texture);
	
	// make it wrap around
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	// make it filter nicely
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	// and put the image data into the texure object
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGB,GL_UNSIGNED_BYTE, pTex);
}



